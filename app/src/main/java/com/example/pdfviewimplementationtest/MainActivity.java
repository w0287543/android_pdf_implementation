package com.example.pdfviewimplementationtest;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Button assetsBtn;
    Button webBtn;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        assetsBtn = findViewById(R.id.assetsBtn);
        webBtn = findViewById(R.id.webBtn);
        context = this;


        assetsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentAsset = new Intent(context, AssetViewActivity.class);
                startActivity(intentAsset);
            }
        });

        webBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentWeb = new Intent(context, WebViewActivity.class);
                startActivity(intentWeb);
            }
        });

    }
}
