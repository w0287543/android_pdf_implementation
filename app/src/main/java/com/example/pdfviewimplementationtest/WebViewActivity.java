package com.example.pdfviewimplementationtest;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class WebViewActivity extends AppCompatActivity {

    ListView listView;
    String [] pdfURLs = {
            "https://www.woundscanada.ca/docman/public/wound-care-canada-magazine/wcc-2019-v17-no1/1415-wcc-spring-2019-v17n1-final-2/file",
            "https://www.woundscanada.ca/docman/public/wound-care-canada-magazine/2018-vol-16-no-2/1381-wcc-winter-2018-v16n2-r1-final/file"
    };
    String [] pdfTitles = {
            "Volume 17 - Issue 1",
            "Volume 16 - Issue 2"
    };





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);

        listView = findViewById(R.id.pdfListView);//link the listviews

        //make the array list adapter to display in the listview
        ArrayList<String> pdfList = new ArrayList<>();
        for(int i = 0; i < pdfURLs.length; i++){
            pdfList.add(pdfTitles[i]);//populate the list with the titles of the PDFs
        }
        final ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_2, android.R.id.text1, pdfList);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String pdfURL = pdfURLs[position];
                Intent intent = new Intent(WebViewActivity.this, WebviewDisplay.class);
                intent.putExtra("pdfURL", pdfURL);//pass in the file id selected
                startActivity(intent);
            }
        });
    }
}
