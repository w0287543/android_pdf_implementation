package com.example.pdfviewimplementationtest;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class AssetViewActivity extends AppCompatActivity {

    //the name of the file
    String[] pdfFiles = {
            "Prevention-and-Management-of-Burns.pdf",
            "1.pdf",
            "2.pdf",
            "3.pdf",
            "4.pdf",
            "5.pdf"
    };
    //the title to display in the list view
    String[] pdfTitles = {
            "Prevention and Management of Burns",
            "One",
            "Two",
            "Three",
            "Four",
            "Five"
    };

    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_asset_view);
        listView = findViewById(R.id.pdfListView);//link the listviews

        //make the array list adapter to display in the listview
        ArrayList<String> pdfList = new ArrayList<>();
        for(int i = 0; i < pdfFiles.length; i++){
            pdfList.add(pdfTitles[i]);//populate the list with the titles of the PDFs
        }
        final ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_2, android.R.id.text1, pdfList);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String pdfFile = pdfFiles[position];
                Intent intent = new Intent(AssetViewActivity.this, AssetDisplayActivity.class);
                intent.putExtra("pdfFile", pdfFile);//pass in the file id selected
                startActivity(intent);
            }
        });

    }
}
